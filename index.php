<?php

require 'simple_html_dom.php';

// The URL to the main MEPs portal
define('BASE_URL' , 'https://www.europarl.europa.eu/meps/en/');


// Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_NOTICE);

// Are we testing the script, or do we want the full list
$testing_mode = true;

// The tags for each MEP
$tags = ["name", "id", "country", "group", "email", "brx", "str", "twitter"];

// The different committees each MP can be in
$committees = [
	'LIBE', 'JURI','ITRE','IMCO','CULT','AFET',
	'DROI','SEDE','DEVE','INTA','BUDG','CONT',
	'ECON','FISC','EMPL','ENVI','TRAN','REGI',
	'AGRI','PECH','AFCO','FEMM','PETI'];

// The different roles for each MP
$roles = ["Chair", "Vice-Chair", "Member", "Substitute"];

// Output type, CSV or JSON
$output_type = "json"; // "json" if you want JSON

$output = [];

function Main($testing_mode,$tags,$committees,$roles,$output_type){
	echo "----------------------------------------------------\n";
	echo "          ~~ ParlScrap - UE MEP Scraper ~~\n";
	echo "----------------------------------------------------\n";
	echo "GPLv3 - Coded by geeks @ La Quadrature du Net - 2021\n";
	echo "\n";

	// We are getting the list of every MEP from the XML document.
	$meps_source = get_list_of_meps($testing_mode);
	$all_meps = [];

	// For every MEP we get, we are adding them to the list of every mep
	foreach($meps_source->find("mep") as $mep_source){
	 array_push($all_meps,get_mep_information($mep_source, $tags, $committees, $roles));
	}

	// print_r($all_meps);

	if ($output_type == "json") {
		output_to_json($all_meps);
	} elseif ($output_type == "csv") {
		output_to_csv($all_meps);
	} else {
		echo ">> Found " . count($all_meps) . " MEPs, outputing them :\n";
		print_r($all_meps);
	}

	// The first line of the CSV with the tags
	// $output[0] = join(",", $tags)  . "," . join(",", $committees);
	// $output = join("\n\r", $output);
	//
	// file_put_contents("meps". strtolower(date("-M-Y_")) . ".csv", $output);
	//
	// header("Content-type: application/octet-stream");
	// header("Content-disposition: attachment;filename=meps". strtolower(date("-M-Y")) . ".csv");
	// echo $output;
}

function get_list_of_meps($testing_mode){
	// Get the full HTML page from the XML document,
	if ($testing_mode == true) {
		echo ">> We are in testing more, we will only be getting the MEPs whose name starts with Y.\n";
		try {
			$meps_source = file_get_html(BASE_URL . 'full-list/xml/y');
		} catch (\Exception $e) {
			echo ">> Error, could not MEPs from URL " . BASE_URL . " : " . $e;
		}

	} else {
		echo ">> Getting every MEP...\n";
		// pour tous les députés, sauf que ça bloque au milieu de la lettre K, je sais pas pourquoi, car ça marche très bien quand je focus juste sur K avec l'option ci-dessus
		try {
			$meps_source = file_get_html(BASE_URL . 'full-list/xml');
		} catch (\Exception $e) {
			echo ">> Error, could not MEPs from URL " . BASE_URL . " : " . $e;
		}	}

	return $meps_source;
}

function get_mep_information($mep_source, $tags, $committees, $roles){

	// We start by getting basic information from the XML
	$mep = [];
	$mep["name"] = $mep_source->find('fullName',0)->plaintext;
	$mep["id"] = $mep_source->find('id',0)->plaintext;
	$mep["country"] = $mep_source->find('country',0)->plaintext;
	$mep["politicalgroup"] = $mep_source->find('politicalGroup',0)->plaintext;

	echo ">> Found " . $mep["name"] . " with ID " . $mep["id"] . " from " . $mep["country"] . " affiliated with " . $mep["politicalgroup"] . "\n";

	echo ">> Getting their web page for extra information..." ."\n";

	// We are going to get more information from the MEP's web page
	try {
		$mep_page = file_get_html(''. BASE_URL . '' . $mep["id"]);
	} catch (\Exception $e) {
		echo ">> Could not get more information from the MEP's page : " .$e ."\n" ;
		return $mep;
	}


	// email
	try {
		$mail = $mep_page->find('.link_email',0)->href;
	} catch (\Exception $e) {
		echo ">> Could not find any email on this web page : " . $e ."\n";
		return $mep;
	}

	$mail = str_replace("[dot]", ".", $mail);
	$mail = str_replace("[at]", "@", $mail);
	$mail = substr($mail, 7);
	$mail = strrev($mail);
	$mep["mail"] = $mail;


	// phone number
	$mep["phone"] = [];
	try {
		$brx = $mep_page->find('.card', 0)->find('a',0)->href;
	} catch (\Exception $e) {

	}

	array_push($mep["phone"], str_replace(' ', '', substr($brx, 4)));

	if ($mep_page->find('.card', 1) != "")
	{
		$stra = $mep_page->find('.card', 1)->find('a',0)->href;
		array_push($mep["phone"], str_replace(' ', '', substr($stra, 4)));
	}


	// twitter
	try {
		$twitter = $mep_page->find('.link_twitt',0)->href;
		if ($twitter != null) {
			$twitter = substr($twitter, 20);
			$mep["twitter"] = $twitter;
		} else {
			$mep["twitter"] = "";
		}
	} catch (\Exception $e) {
		$mep["twitter"] = "";
		echo ">> Could not get Twitter info for " . $mep["name"] . " :" . $e;
	}

	// For each committee, we are going to find if the MEP has a role.
	// BEWARE ! Note that the 'write-back' feature of the ampersand persists even after the loop has finished: resetting $val to a new value will change the last element in $val, which is often unexpected.

	// We extend the array with every possible committee
	$mep["committees"] = [];
	foreach ($committees as $key => $value) {
		$mep["committees"][$value] = "";
	}

	// array_pad($mep["committees"], sizeof($tags) + sizeof($committees), "");

	foreach($mep_page->find('.erpl_meps-status') as $status)
	{
		foreach($roles as $role)
		{
			if ($status->find("h4",0)->plaintext == $role)
				foreach($status->find('.erpl_badge') as $badge)
				{
					$com = $badge->plaintext;
					if (in_array($com, $committees))
					{
						$index = array_search($com, $committees);
						$mep["committees"][$com] = strtolower($role);
					}
				}
		}
	}
	return $mep;
}

function output_to_csv($mep){

}

function output_to_json($mep){
	echo ">> Saving to file...\n";
	$filname = "MEPS-" . date("c") . ".json";
	try {
		file_put_contents( $filname , json_encode($mep));
	} catch (\Exception $e) {
		echo ">> Erro while saving file : " . $e . "\n";
	}

	echo "Saved file to " . $filname . "\n";

}

// Let's run this script !
Main($testing_mode,$tags,$committees,$roles,$output_type);
?>
