# ParlScrap

> PHP Script to download a .json file of every EU MEP.

## Usage

On a computer with PHP >=7.4 installed, run `php index.php`. You will get a file with today's date in the filename, for easy classification.

## Improvements to be made

- Use an asynchronous download of every MEP
- More output formats
- Added the picture of every MEP
- More user-friendly settings

## Credit

Original work by Oncela, additionnal code by Nono.

## Licence

> ParlScrap - PHP Script to download a .json file of every EU MEP.  
> Copyright (C) 2021 La Quadrature Du Net  
> This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  
> This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.  
> You should have received a copy of the GNU General Public License along with this program. If not, see <http s ://www.gnu.org/licenses/>.  
